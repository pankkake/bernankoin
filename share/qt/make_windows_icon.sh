#!/bin/bash
# create multiresolution windows icon
ICON_SRC=../../src/qt/res/icons/bernankoin.png
ICON_DST=../../src/qt/res/icons/bernankoin.ico
convert ${ICON_SRC} -resize 16x16 bernankoin-16.png
convert ${ICON_SRC} -resize 32x32 bernankoin-32.png
convert ${ICON_SRC} -resize 48x48 bernankoin-48.png
convert ${ICON_SRC} -resize 64x64 bernankoin-64.png
convert bernankoin-32.png ${ICON_SRC} bernankoin-64.png bernankoin-48.png bernankoin-32.png bernankoin-16.png ${ICON_DST}

