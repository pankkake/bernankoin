Bernankoin
==========

The coin to end the economic crisis.

    :'::.:::::.:::.::.:
    : :.: ' ' ' ' : :':
    :.:     _.__    '.:
    :   _,^"   "^x,   :
    '  x7'        `4,
     XX7            4XX
     XX              XX
     Xl ,xxx,   ,xxx,XX
    ( ' _,+o, | ,o+,"
     4   "-^' X "^-'" 7
     l,     ( ))     ,X
     :Xx,_ ,xXXXxx,_,XX
      4XXiX'-___-`XXXX'
       4XXi,_   _iXX7'
      , `4XXXXXXXXX^ _,
      Xx,  ""^^^XX7,xX
    W,"4WWx,_ _,XxWWX7'
    Xwi, "4WW7""4WW7',W
    TXXWw, ^7 Xk 47 ,WH
    :TXXXWw,_ "), ,wWT:
    ::TTXXWWW lXl WWT:
